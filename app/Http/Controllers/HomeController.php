<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function dataProduk()
    {
        return view('awal');
    }
    public function formProduk()
    {
        return view('formproduk');
    }
    public function daftarBarang()
    {
        return view('daftarbarang');
    }
}
