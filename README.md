1. git clone link
2. Install library menggunakan perintah composer install
3. Membuat file .env dengan perintah copy .env.example .env
4. Kemudian isikan semua pengaturan yang perlu kamu masukkan, biasanya yang penting adalah pengaturan koneksi database.
5. Jalankan perintah php artisan key:generate
6. Jalankan perintah php artisan migrate:refresh --seed
7. Jalankan perintah php artisan storage:link
8. OK
