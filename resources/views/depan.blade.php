@extends('includes.layout')
@section('content')
<main role="main">

    <div class="container-fluid">
        <section id="beranda">
            <div class="row">
                <div class="col-12">
                    <img class="" style=" width: 100%; height: 750px"
                        src="{{ asset('img/farm.jpg') }}">
                    <h1 style="position: absolute;transform: translate(-50%,-50%); bottom: 57%; left: 50%;"
                        class="text-bold">Lets Make Your Dream in Here</h1>
                </div>
            </div>
        </section>
        <br>
        <br>
        <section style="background-color: #F0FFFF;" id="prestasi" class="">
            <div class="container">
                <!-- Three columns of text below the carousel -->
                <div class="row py-5">
                    <h1 class="text-center col-12 py-5 text-black font-weight-bolder">#Trending Item</h1>
                    <div class="col-lg-4">
                        <img class="rounded col-11" src="{{ asset('img/domba.jpg') }}"
                            alt="Generic placeholder image" width="100" height="200">
                        <p href="" class="col-11"> <br> </p>
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded col-11" src="{{ asset('img/kambing.jpg') }}"
                            alt="Generic placeholder image" width="140" height="200">
                        <p class="col-11">Kambing Garut <br> Rp 4.000.000</p>
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <img class="rounded col-11" src="{{ asset('img/domba.jpg') }}"
                            alt="Generic placeholder image" width="140" height="200">
                        <p class="col-11">Domba Garut <br> Rp 3.000.000</p>
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div>
        </section>
        <br>
        <br>
        <br>

        <div id="carouselExampleControls" class="carousel slide py-5 my-3" data-ride="carousel">
            <h1 class="text-center py-2">#Testimoni</h1>
            <div class="container py-5 my-3">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                                <h3 class="py-4 text-center">Bio Abidzar</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya berkualitas</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                                <h3 class="py-4 text-center">Budi Sudirman</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya berkualitas</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                                <h3 class="py-4 text-center">Anne Marie</h3>
                                                <h5 class="py-2">Sangat Memuaskan pelayanan baik dan ramah serta hewannya berkualitas</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</main>
@endsection