@extends('includes.layout')
@section('content')
<div class="container">
    <div class="col-md-12">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1 class="h1 pb-4 py-3">Tambah Data Produk</h1>
            <div class="card mt-2">
                <div class="card-header">{{ __('DataProduk') }}</div>
                <div class="card-body">
                	@if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
                	<form action="formproduk" method="POST" enctype="multipart/form-data">
                		@csrf
                		<div class="row">
                		<div class="col-6">
                        <div class="form-group">
                        <label for="jenis">Jenis</label>
                        <select class="form-control form-control-sm" name="jenis">
                            <option value="">Pilih jenis produk</option>
                            <option value="Sapi">Sapi</option>
                            <option value="Kambing">Kambing</option>
                            <option value="Domba">Domba</option>
                        </select>
                    	</div>
                        <div class="form-group">
                        	<label for="type">Type</label>
                        	<select name="type" id="" class="form-control">
	                            <option value="" >Pilih type produk</option>
	                            <option value="Type A">type A</option>
	                            <option value="Type B">type B</option>
	                            <option value="Type C">type C</option>
	                            <option value="Type D">type D</option>
                            </select>
                        </div>
                        <div class="form-group">
                        	<label for="gender">JenisKelamin</label>
                        	<select name="gender" id="" class="form-control">
	                            <option value="" >Pilih jenis kelamin</option>
	                            <option value="Jantan">Jantan</option>
	                            <option value="Betina">Betina</option>
                            </select>
                        </div>
                    </div>
                        <div class="col-6">
                        <div class="form-group">
                        	<label for="hight">Tinggi</label>
                        	<input type="text" name="hight" class="form-control">
                        </div>
                        <div class="form-group">
                        	<label for="wight">Berat</label>
                        	<input type="text" name="wight" class="form-control">
                        </div>
                    </div>
                </div>
                <br>
                    <button type="submit" class="btn btn-primary">Create</button>
                    
                	</form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection