@extends('layouts.master')

@section('title')
Jarvis Farm
@endsection()

@section('title-content')
Farm
@endsection()

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">@yield('title-content')</h1>
        <a href="{{ route('produk.create') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus fa-sm text-white-50"></i> Add Product</a>
    </div>

    <!-- DataTales Example -->
<div class="card shadow mb-4">
<div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">Data Product</h6>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>id</th>
                    <th>jenis</th>
                    <th>type</th>
                    <th>berat</th>
                    <th>tinggi</th>
                    <th>gender</th>
                    <th>aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($produks as $produk)
                <tr>
                    <td>{{ $produk->id }}</td>
                    <td>{{ $produk->jenis }}</td>
                    <td>{{ $produk->type }}</td>
                    <td>{{ $produk->hight }}</td>
                    <td>{{ $produk->wight }}</td>
                    <td>{{ $produk->gender}}</td>
                    <td>
                        <a href="{{ route('produk.edit', $produk->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></a>
                        <form action="{{ route("produk.destroy", $produk->id) }}" method="POST" class="form-inline">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-sm btn-danger rounded-0" onclick="return confirm('Apakah anda yakin untuk menghapus data ini?')" style=""><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection()
