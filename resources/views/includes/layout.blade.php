<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		{{-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> --}}
		{{-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}"> --}}
		
		<title>Jarvis Farm</title>
		<!-- Font Awesome Icons -->
		<script src="https://kit.fontawesome.com/d54b50045e.js"></script>
		{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
		<!-- Fonts -->
		<link rel="dns-prefetch" href="//fonts.gstatic.com">
		<!-- Plugin CSS -->
		<link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
		<!-- Theme CSS - Includes Bootstrap -->
		<link href="{{ asset('css/creative.css') }}" rel="stylesheet">
		<!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js">
    </script>
	</head>
	<body id="page-top">
		{{-- @include('includes.sidebar') --}}
		@include('includes.navbar')
		<body>
			@yield('content')
		</body>
		@include('includes.footer')
		<!-- Bootstrap core JavaScript -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<!-- Plugin JavaScript -->
		<script src="{{ asset('js/jquery-easing/jquery.easing.min.js') }}"></script>
		<script src="{{ asset('js/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
		<!-- Custom scripts for this template -->
		<script src="{{ asset('js/creative.min.js') }}"></script>
	</body>
</html>
