<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('depan');
});

Route::prefix('admin')->namespace('Admin')->group(static function() {
        Route::resource('produk', 'ProductController');
});

Route::get('awal','HomeController@dataProduk')->name('awal');
Route::get('formproduk','HomeController@formproduk')->name('formproduk');
Route::get('daftarbarang','HomeController@daftarBarang')->name('daftarbarang');

Route::get('depan',[ProdukController::class,'dataProduk']);
// Route::POST('/produk','ProdukController@store')->name('produk');
// Route::get('awal','ProdukController@create')->name('formproduk');
// Route::get('/create','ProdukController@create')->name('create');
Route::POST('formproduk',[ProdukController::class,'store']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::prefix('user')->group(function () {
//     Route::resource('produk', 'ProdukController');
// });